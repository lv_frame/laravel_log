<?php


namespace Anchu\LaravelLog\Controllers;

use Anchu\Restful\Controllers\RestController;

class LogController extends RestController
{
    public function phpinfo()
    {
        // 不配置token，不返回信息
        if (env('LOG_TOKEN', '0') == '0') {
            return 'token error';
        }

        if (env('LOG_TOKEN', '0') == request()->query('token', '1')) {
            return phpinfo();
        } else {
            return 'token error';
        }
    }
}
