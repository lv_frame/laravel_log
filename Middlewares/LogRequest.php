<?php

namespace Anchu\LaravelLog\Middlewares;

use Anchu\LaravelLog\Models\LogRequestModel;
use Closure;
use Illuminate\Http\Request;

class LogRequest
{
    /**
     * 处理传入的请求。
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    /**
     * 在响应发送到浏览器后处理任务。
     *
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Http\Response $response
     * @return void
     */
    public function terminate($request, $response)
    {
        LogRequestModel::create($response);
    }
}
